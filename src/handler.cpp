#include "../include/handler.h"

handler::handler()
{
    //ctor
}
handler::handler(utility::string_t url):m_listener(url)
{
    m_listener.support(methods::GET, std::bind(&handler::handle_get, this, std::placeholders::_1));
    m_listener.support(methods::PUT, std::bind(&handler::handle_put, this, std::placeholders::_1));
    m_listener.support(methods::POST, std::bind(&handler::handle_post, this, std::placeholders::_1));
    m_listener.support(methods::DEL, std::bind(&handler::handle_delete, this, std::placeholders::_1));

}
handler::~handler()
{
    //dtor
}

void handler::handle_error(pplx::task<void>& t)
{
    try
    {
        t.get();
    }
    catch(...)
    {
        // Ignore the error, Log it if a logger is available
    }
}


//
// Get Request 
//
void handler::handle_get(http_request message)
{

    ucout <<  message.to_string() << endl;

    ucout <<  "relative uri path : "<< message.relative_uri().path() << endl; // path
    ucout <<  "relative uri query : "<< message.relative_uri().query() << endl;//query

    //relative path is required at the first stage to differenciate
    auto paths = http::uri::split_path(http::uri::decode(message.relative_uri().path()));
    if (paths.empty())
    {
        message.reply(status_codes::OK, U("empty path"));
        return;
    }

    utility::string_t wtfirstpath = paths[0];
    const utility::string_t firstpath = wtfirstpath;


    //parameters can be retrived via the query. which helps to differenciate 
    std::map<utility::string_t, utility::string_t> query = uri::split_query(uri::decode(message.request_uri().query()));

    auto cntEntry = query.find("name");

    if (cntEntry != query.end() && !cntEntry->second.empty())
    {
        utility::ostringstream_t os;
        os << U(" the name is :") << U(cntEntry->second);
        message.reply(status_codes::OK, os.str());
    }
    else
    {
        message.reply(status_codes::Forbidden, U("No name provided."));
    }


    message.reply(status_codes::OK, firstpath);

    return;

};

//
// A POST request
//
void handler::handle_post(http_request message)
{
    ucout <<  message.to_string() << endl;


     message.reply(status_codes::OK,message.to_string());
    return ;
};

//
// A DELETE request
//
void handler::handle_delete(http_request message)
{
     ucout <<  message.to_string() << endl;

        string rep = U("WRITE YOUR OWN DELETE OPERATION");
      message.reply(status_codes::OK,rep);
    return;
};


//
// A PUT request 
//
void handler::handle_put(http_request message)
{
    ucout <<  message.to_string() << endl;
     string rep = U("WRITE YOUR OWN PUT OPERATION");
     message.reply(status_codes::OK,rep);
    return;
};
