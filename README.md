
RestAPI to support put, get, post and delete methods. deps -> cpprestsdk(git@github.com:Microsoft/cpprestsdk.git)

What is this repository for?
sample Rest API in c++

How do I get set up?
install deps sudo apt-get install libcpprest-dev

How to compile : inside build/
cmake ../
make
go to build/bin ./restapi
